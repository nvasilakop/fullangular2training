// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    // apiKey: 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx',
    // authDomain: 'fcc-book-trading-173021.firebaseapp.com',
    // databaseURL: 'https://fcc-book-trading-173021.firebaseio.com',
    // projectId: 'fcc-book-trading-173021',
    // storageBucket: 'fcc-book-trading-173021.appspot.com',
    // messagingSenderId: '763399536402'
    apiKey: "AIzaSyC9eeHLXeTQd-qKnNry3cctflPQkdlgrgY",
    authDomain: "chatbox-signalr.firebaseapp.com",
    databaseURL: "https://chatbox-signalr.firebaseio.com",
    projectId: "chatbox-signalr",
    storageBucket: "chatbox-signalr.appspot.com",
    messagingSenderId: "442937353541"
}
};

/*cd
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
