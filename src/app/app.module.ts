import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {RoutingModule} from './Modules/routing/routing.module';

import { AppComponent } from './app.component';
import { LoginComponent } from './Components/login/login.component';
import { CreateUserComponent } from './Components/create-user/create-user.component';
import { FakeComponent } from './Components/fake/fake.component';
import { BottomComponent } from './Components/bottom/bottom.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateUserComponent,
    FakeComponent,
    BottomComponent
  ],
  imports: [
    BrowserModule,ReactiveFormsModule,RoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
