import {User} from '../Classes/User';

export  const MockUsers: User[] = [
    {id : 1 , name : 'Nvasilakop', age : 27 , email : 'nvasilakop@gmail.com' ,password:'0123' },
    {id : 55 , name : 'Paloukari12', age : 26 ,email : ' ',password:'921391'},
    {id : 15 , name : 'Paloukari', age : 25,email : ' ',password:'14141'},
    {id : 65, name : 'Thanatost', age : 24,email : ' ',password:'1314'},
  ];
  