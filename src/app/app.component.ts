import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  selected :boolean = false ;

  onChangeFlag(): void{
    this.selected = !this.selected;
  }
}
