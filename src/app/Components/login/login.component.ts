import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {FormGroup,FormControl} from '@angular/forms';
import {User} from '../../Classes/User';
import {MockUsers} from '../../MockData/MockUsers';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user : User;
  emails : string[] = [];
  hiddeLogin : boolean ;
  @Output() isExistingUser = new EventEmitter<boolean>();
  loginForm : FormGroup;
  takeTheFlag : boolean;

  isNewUser : boolean;
  constructor() {
    this.user = new User();
    this.loginForm = new FormGroup({
      password : new FormControl(this.user.password),
      email : new FormControl(this.user.email)
    });
    this.hiddeLogin = !this.hiddeLogin;
   }

  ngOnInit() {  
    for(let i=0;i <= MockUsers.length;i++){
      this.emails.push(MockUsers[i].email)
    }
  }

  loginUser() : void {
    this.user.password = this.loginForm.value['password'];
    this.user.email = this.loginForm.value['email'];
  this.hiddeLogin = MockUsers.filter(r => r.email== this.user.email && r.password ==this.user.password).length > 0;
  console.log(this.user);
  console.log(this.hiddeLogin);
  if(!this.hiddeLogin){
    this.isNewUser = true;
  }
  else{
    this.isNewUser = false;
  }
  }

  takeTheNextFlag(nextFlag : boolean):void{
    this.takeTheFlag = nextFlag;
    console.log(nextFlag);
  }
}
