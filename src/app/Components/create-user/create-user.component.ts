import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  constructor() {
    this.createUser();
  }
  createUserNew: boolean;
  nextFlag : boolean;
  @Input() isExistedUser: boolean;
  ngOnInit() {
    console.log(this.isExistedUser);

  }

  createUser(): void {
    this.createUserNew = this.isExistedUser;
    console.log(this.isExistedUser);
  }
  takeMeBack(value : boolean) : void {
    this.nextFlag = value;
  }
}
