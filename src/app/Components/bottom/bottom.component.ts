import { Component, OnInit, Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-bottom',
  templateUrl: './bottom.component.html',
  styleUrls: ['./bottom.component.css']
})
export class BottomComponent implements OnInit {

  nextFlag :boolean;
  @Output() next = new EventEmitter<boolean>();
  constructor() { }

  ngOnInit() {
  }

  toggleFront() : void {
    this.nextFlag = true;
    this.next.emit(this.nextFlag);
    console.log(this.nextFlag);
  }

  toggleBack():void{
    this.nextFlag = false;
    this.next.emit(this.nextFlag);
    console.log(this.nextFlag);
  }
}
